<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use PDF;

class GasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $receipts = \App\Receipt::paginate(10);

        return view('/logs', compact('receipts'));
    }

    public function computePurchase(Request $request)
    {
        $receipt = new \App\Receipt;

        if ($request->query('gas') == 'diesel') {
            $gas_price = 38.00;
        } elseif ($request->query('gas') == 'unleaded') {
            $gas_price = 44.00;
        } else {
            $gas_price = 50.00;
        }

        $total = $gas_price * $request->liter;
        $vat = $total * .12;

        $receipt->gasoline_type = $request->query('gas');
        $receipt->gasoline_price = $gas_price;
        $receipt->liters = $request->liter;
        $receipt->VAT = $vat;
        $receipt->total = $total;


        $receipt->save();


        $receipts = \App\Receipt::orderBy('created_at', 'desc')->take(1)->get();

        return view('/home', compact('receipts'))->with('success', 'your purchase is successful');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $query = $request->query();

        if ($query['col'] == 'name') {
            $col = 'gasoline_type';
        } else {
            $col = 'created_at';
        }

        $receipts = \App\Receipt::orderBy($col, $query['sort'])->paginate(10);

        return view('/logs', compact('receipts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $receipts = \App\Receipt::orderBy('created_at', 'desc')->take(1)->get();

        return view('/home', compact('receipts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $receipt = \App\Receipt::find($id);

        $pdf = PDF::loadView('invoice', compact('receipt'));
        return $pdf->download('invoice.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function report()
    {
        $receipts = \App\Receipt::all();

        $pdf = PDF::loadView('invoice_summary', compact('receipts'));
        return $pdf->download('invoice.pdf');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
