<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Gasoline Store</title>

</head>

<body>
<style>
    .center{
        text-align: center
    }
    .table{
        text-align: center;
        width: 100%;

    }

table, td, th {
  border: .5px solid black;
}

    </style>

    <h2 class="center">Invoice Summary</h2>
    <table class="table" border="2">
        <thead>
            <tr>
                <th>id</th>
                <th>Gas Type</th>
                <th>Gas Price</th>
                <th>Liters</th>
                <th>Vat</th>
                <th>Total</th>
                <th>Purchase Date</th>
            </tr>
        </thead>
        <tbody>
            @php
            $counter = 1;
            @endphp
            @forelse ($receipts as $receipt)
            <tr>
                <td>{{ $counter++ }}</td>
                <td>{{ $receipt->gasoline_type }}</td>
                <td>{{ $receipt->gasoline_price }}</td>
                <td>{{ $receipt->liters }}</td>
                <td>{{ $receipt->VAT }}</td>
                <td>{{ $receipt->total }}</td>
                <td>{{ $receipt->created_at }}</td>
            </tr>
            @empty

            @endforelse
        </tbody>
    </table>
</body>

</html>
