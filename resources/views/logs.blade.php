<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Gasoline Store</title>

    {{-- bootstrap CDN --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css">

    {{-- CSS link --}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>
<body>

    <div class="wrapper">

        {{-- navbar --}}
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <div class="container">
                <a class="navbar-brand" href="#">Gasoline Store</a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                aria-expanded="false" aria-label="Toggle navigation"></button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="/"><i class="fa fa-home" aria-hidden="true"></i> Home </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/history"><i class="fa fa-book" aria-hidden="true"></i> History <span class="sr-only">(current)</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row mt-5">

                {{-- Side Panel --}}
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Order By:</h5>
                            <ul>
                                <li><a href="/search/name?sort=asc&col=name"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i> A - Z</a></li>
                                <li><a href="/search/name?sort=desc&col=name"><i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Z - A</a></li>
                                <li><a href="/search/points?sort=desc&col=created"><i class="fas fa-sort-numeric-up    "></i> Newest</a></li>
                                <li><a href="/search/points?sort=asc&col=created"><i class="fas fa-sort-numeric-down    "></i> Oldest</a></li>
                            </ul>
                            <hr>

                            <a class="btn btn-block btn-primary" href="{{action('GasController@report')}}">Download Report</a>
                        </div>
                    </div>
                </div>

                {{-- main content --}}
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Invoice Management</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Gas Type</th>
                                        <th>Gas Price</th>
                                        <th>Liters</th>
                                        <th>Vat</th>
                                        <th>Total</th>
                                        <th>Purchase Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $counter = 1;
                                    @endphp
                                    @forelse ($receipts as $receipt)
                                    <tr>
                                        <td>{{ $counter++ }}</td>
                                        <td>{{ $receipt->gasoline_type }}</td>
                                        <td>{{ $receipt->gasoline_price }}</td>
                                        <td>{{ $receipt->liters }}</td>
                                        <td>{{ $receipt->VAT }}</td>
                                        <td>{{ $receipt->total }}</td>
                                        <td>{{ $receipt->created_at }}</td>
                                    </tr>
                                    @empty

                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-center">
                            {{ $receipts->appends($_GET)->links() }}
                        </div>
                    </div>
                </div>

            </div>
        </div>


        {{-- footer --}}
        <div class="push"></div>
    </div>
    <footer class="footer bg-primary">&copy; Reymar Enteria 2019</footer>


    {{-- Javascript link --}}
    <script src="{{ asset('js/script.js') }}"></script>

    {{-- bootstrap CDN --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>


</body>
</html>
