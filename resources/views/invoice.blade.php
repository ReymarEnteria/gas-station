<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Gasoline Store</title>

</head>
<body>
    <div id="showScroll" class="invoice-container">
        <div class="receipt">
            <h1 class="logo">Gas Mo To</h1>
            <div class="address">
                168 Lincoln St. Sta Mesa, ML
            </div>
            <div class="center">
                Helped by: Reymar
            </div>
            <hr>
            <div class="survey bold">
                <p>Date of Purchase</p>
                <p class="surveyID">{{ $receipt->created_at }}</p>
            </div>
                <div class="item">
                    <p> Gas Type: <span class="space"></span> {{ $receipt->gasoline_type }}</p>
                    <p> Liter/s: <span class="space"></span> {{ $receipt->liters }}</p>
                </div>
                <hr>
                <div class="item">
                    <p> VAT: <span class="space"></span>{{ $receipt->VAT }}</span></p>
                    <p> Total: <span class="space"></span>{{ $receipt->total }}</span></p>
                </div>
            <div>
                <div class="center">Account #</div>
                <div class="center">************4023 BPI</div>
            </div>
            <hr>
            <div class="center bold">
                Returns with receipt, subject to GMT Return Policy, thru 12/04/2022
            </div>
            <div>
                <div class="bold">Trip Summary:</div>
                    <p class="item">Today You Saved:  <span class="space"></span>  .00</p>
                    <p class="item">Savings Value: <span class="space"></span>   0%</p>
            </div>
                <div class="center">
                    *************************************
                </div>
                <p class="center">
                    We would love to hear your feedback on your recent experience with us. This survey will only take 1 minute to complete.
                </p>
                <h3 class="center">Share Your Feedback</h3>
                <h4 class="center">www.GasMoTo.com</h4>
                <h5 class="center">Reymar Enteria</h5>
            </div>
    </div>

</body>

    <style>
        .space{
            padding-left: 80px;
        }
        .item{
            margin-left: 30px;
        }

        .bold {
            font-weight: bold;
        }

        .center {
            text-align: center;
        }

        .receipt {
            width: 400px;
            margin: 0 auto;
            border: 1px solid black;
        }

        .logo {
            text-align: center;
            padding: 20px;
        }
        .address {
            text-align: center;
            margin-bottom: 10px;
        }

        .survey {
            text-align: center;
            margin-bottom: 12px;
        }

        .survey .surveyID {
            font-size: 20px;
        }

    </style>
</html>
