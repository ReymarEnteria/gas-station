<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Gasoline Store</title>

    {{-- bootstrap CDN --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css">

    {{-- CSS link --}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>
<body>

    <div class="wrapper">

        {{-- navbar --}}
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <div class="container">
                <a class="navbar-brand" href="/">Gasoline Store</a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                aria-expanded="false" aria-label="Toggle navigation"></button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link" href="/"><i class="fa fa-home" aria-hidden="true"></i> Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/history"><i class="fa fa-book" aria-hidden="true"></i> History</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        @if(session()->has('success'))
        <div class="alert alert-success text-center">
            {{ session()->pull('success') }}
        </div>
        @endif


        {{-- body --}}
        <div class="container mt-3">
            <div class="row">

                {{-- Orders --}}
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Fuel Type</h3>
                        </div>
                        <div class="card-body">

                            {{-- Gasoline Choices --}}
                            <div class="card-deck">

                                {{-- Unleaded --}}
                                <div class="card">
                                    <img class="card-img-top" src="{{ asset('images/unleaded.png') }}" alt="" data-toggle="modal" data-target="#Unleaded">
                                </div>

                                {{-- Diesel --}}
                                <div class="card">
                                    <img class="card-img-top" src="{{ asset('images/diesel.png') }}" alt="" data-toggle="modal" data-target="#Diesel">
                                </div>

                                {{-- premium --}}
                                <div class="card">
                                    <img class="card-img-top" src="{{ asset('images/premium.png') }}" alt="" data-toggle="modal" data-target="#Premium">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {{-- Preview --}}
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Recent Purchase</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <p> Date & Time:</p>
                                    <p> Gas Type:</p>
                                    <p> Liter/s:</p>
                                </div>
                                <div class="col-md-7 text-right">
                                    @forelse ($receipts as $receipt)
                                    <p>{{ $receipt->created_at }}</p>
                                    <p>{{ $receipt->gasoline_type }}</p>
                                    <p>{{ $receipt->liters }}</p>
                                    @empty

                                    @endforelse
                                </div>
                            </div>
                            <hr>
                            <div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p> VAT:</p>
                                        <p> Total:</p>
                                    </div>
                                    <div class="col-md-8 text-right">
                                        @forelse ($receipts as $receipt)
                                        <p>{{ $receipt->VAT }}</p>
                                        <p>{{ $receipt->total }}</p>
                                        @empty

                                        @endforelse
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            @forelse ($receipts as $receipt)
                            <a class="btn btn-block btn-primary" href="{{action('GasController@download', $receipt->id)}}">Download Invoice</a>
                            @empty
                            <button class="btn btn-block btn-primary" type="button">Download Invoice</button>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Unleaded Modal -->
        <div class="modal fade" id="Unleaded" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Fuel Type: Unleaded</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/purchase?gas=unleaded" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="unleadedInput">Enter Number of Liters</label>
                                <input type="number" min="1" class="form-control" id="unleadedInput"  name="liter" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--Diesel Modal -->
        <div class="modal fade" id="Diesel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Fuel Type: Diesel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/purchase?gas=diesel" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="dieselInput">Enter Number of Liters</label>
                                <input type="number" min="1" class="form-control" id="dieselInput" name="liter" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Premium -->
        <div class="modal fade" id="Premium" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Fuel Type: Premium</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/purchase?gas=premium" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="premiumInput">Enter Number of Liters</label>
                                <input type="number" min="1" class="form-control" id="premiumInput" name="liter" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>








        {{-- footer --}}
        <div class="push"></div>
    </div>
    <footer class="footer bg-primary">&copy; Reymar Enteria 2019</footer>


    {{-- Javascript link --}}
    <script src="{{ asset('js/script.js') }}"></script>

    {{-- bootstrap CDN --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>


</body>
</html>
