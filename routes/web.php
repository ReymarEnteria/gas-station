<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/purchase', 'GasController@computePurchase');
Route::get('/', 'GasController@home');

Route::get('/history', 'GasController@index');

Route::get('/search/name', 'GasController@search');
Route::get('/search/points', 'GasController@search');

Route::get('/download_invoice/{id}', 'GasController@download');
Route::get('/download_report', 'GasController@report');
